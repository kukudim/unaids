export const headerMenu = [
  {
    title: "Все проекты",
    link: "#",
  },
  {
    title: "Как это работает",
    link: "#",
  },
  {
    title: "Отчеты",
    link: "#",
  },
  {
    title: "Подать заявку",
    link: "#",
  }
]

export const topCarousel = [
  {
    id: 1,
    title: "Первый в Европе центр доконтактной профилактики ВИЧ",
    description: "Речь, подчеркнул зампред ЦБ, идет о предварительных оценках, так как еще не все активы группы изучены временной администрацией.",
    link: "#project-1",
    video_preview_url: "/images/nik.jpg"
  },
  {
    id: 2,
    title: "Заседания Клуба анонимных перфекционистов",
    description: "Перфекционизм в цифровую эпоху: дар или проклятие?",
    link: "#project-2",
    video_preview_url: "/images/nik.jpg"
  },
  {
    id: 3,
    title: "Первый в Европе центр доконтактной профилактики ВИЧ",
    description: "Речь, подчеркнул зампред ЦБ, идет о предварительных оценках, так как еще не все активы группы изучены временной администрацией.",
    link: "#project-3",
    video_preview_url: "/images/nik.jpg"
  },
  {
    id: 4,
    title: "Заседания Клуба анонимных перфекционистов",
    description: "Перфекционизм в цифровую эпоху: дар или проклятие?",
    link: "#project-4",
    video_preview_url: "/images/nik.jpg"
  },
]

export const recentProjects = [
  {
    id: 1,
    title: "Кирилловская пекарня: хлеб для отдаленных районов Нижегородской области",
    link: "#project-1",
    image_url: "/images/cala.jpg",
    status: 20,
    date: "2019-03-03T19:30:47+00:00"
  },
  {
    id: 2,
    title: "Заседания Клуба анонимных перфекционистов",
    link: "#project-2",
    image_url: "/images/erik.jpg",
    status: 70,
    date: "2019-03-03T19:30:47+00:00"
  },
  {
    id: 3,
    title: "Художественный фильм Михаила Марескина «Вторая Сестра»",
    link: "#project-3",
    image_url: "/images/jake.jpg",
    status: 40,
    date: "2019-03-03T19:30:47+00:00"
  },
  {
    id: 4,
    title: "Детская машинка T-toyz с рулевым управлением",
    link: "#project-4",
    image_url: "/images/felix.jpg",
    status: 20,
    date: "2019-03-03T19:30:47+00:00"
  },
  {
    id: 5,
    title: "Детская машинка T-toyz с рулевым управлением",
    link: "#project-5",
    image_url: "/images/cala.jpg",
    status: 20,
    date: "2019-03-03T19:30:47+00:00"
  },
  {
    id: 6,
    title: "Кирилловская пекарня: хлеб для отдаленных районов Нижегородской области",
    link: "#project-6",
    image_url: "/images/erik.jpg",
    status: 80,
    date: "2019-03-03T19:30:47+00:00"
  },
  {
    id: 7,
    title: "Заседания Клуба анонимных перфекционистов",
    link: "#project-7",
    image_url: "/images/jake.jpg",
    status: 50,
    date: "2019-03-03T19:30:47+00:00"
  },
  {
    id: 8,
    title: "Детская машинка T-toyz с рулевым управлением",
    link: "#project-8",
    image_url: "/images/felix.jpg",
    status: 20,
    date: "2019-03-03T19:30:47+00:00"
  },
  {
    id: 9,
    title: "Кирилловская пекарня: хлеб для отдаленных районов Нижегородской области",
    link: "#project-9",
    image_url: "/images/cala.jpg",
    status: 20,
    date: "2019-03-03T19:30:47+00:00"
  },
  {
    id: 10,
    title: "Заседания Клуба анонимных перфекционистов",
    link: "#project-10",
    image_url: "/images/erik.jpg",
    status: 10,
    date: "2019-03-03T19:30:47+00:00"
  },
  {
    id: 11,
    title: "Художественный фильм Михаила Марескина «Вторая Сестра»",
    link: "#project-11",
    image_url: "/images/jake.jpg",
    status: 40,
    date: "2019-03-03T19:30:47+00:00"
  },
  {
    id: 12,
    title: "Детская машинка T-toyz с рулевым управлением",
    link: "#project-12",
    image_url: "/images/felix.jpg",
    status: 20,
    date: "2019-03-03T19:30:47+00:00"
  },
]

export const submitProject = {
  title: "Есть проект, который требует помощи?",
  image_url: "/images/ivana.jpg",
}
