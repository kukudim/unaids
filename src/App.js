import React, { PureComponent } from 'react';
import 'moment/locale/ru';

import Header from 'components/Header';
import Home from 'pages/Home';
import styles from './App.module.scss';


class App extends PureComponent {
  render() {
    return (
      <div className={styles.wrapper}>
        <Header />
        <Home />
      </div>
    );
  }
}

export default App;
