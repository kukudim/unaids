import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import Button from 'components/Button';
import styles from './SubmitProject.module.scss';
import { submitProject as data } from 'api/mock.js';


class SubmitProject extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
  }

  static defaultProps = {
    className: ''
  }

  render() {
    const { className } = this.props;

    return (
      <div className={cn(className, styles.submitProject)}>
        <div
          className={styles.image}
          style={{ backgroundImage: `url("${data.image_url}")`}}
        >
        </div>
        <div className={styles.info}>
          <div className={styles.title}>
            {data.title}
          </div>
          <Button
            className={styles.submitButton}
            buttonStyle="button"
            appearance="isPrimary"
          >
            Подать заявку
          </Button>
        </div>
      </div>
    )
  }
}

export default SubmitProject;
