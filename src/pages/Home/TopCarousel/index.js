import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Slider from 'react-slick';

import { desktop } from 'components/Responsive';
import Icon from 'components/Icon';
import Button from 'components/Button';
import styles from './TopCarousel.module.scss';
import { topCarousel as data } from 'api/mock.js';


const SliderArrow = ({className, icon, cls, onClick}) => (
  <Button className={cn(className, styles.arrow, styles[cls])} onClick={onClick}>
    <Icon type={icon}/>
  </Button>
)

class TopCarousel extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
  }

  static defaultProps = {
    className: ''
  }

  render() {
    const sliderSettings = {
      dots: false,
      arrows: true,
      speed: 500,
      infinite: true,
      lazyLoad: 'ondemand',
      autoplay: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      cssEase: 'linear',
      prevArrow: <SliderArrow icon="arrowLeft" cls="arrowPrev" />,
      nextArrow: <SliderArrow icon="arrowRight" cls="arrowNext" />,
      customPaging: () => <Button className={styles.dot} classNameInner={styles.dotInner}></Button>,
      dotsClass: `slick-dots ${styles.dots}`,
      responsive: [{
        breakpoint: desktop-1,
        settings: {
          dots: true,
        }
      }]
    }

    const { className } = this.props;
    return (
      <div className={cn(className, styles.topCarousel)}>
        <Slider {...sliderSettings}>
          {data.map(item =>
            <div className={styles.slide} key={item.id}>
              <div className={styles.slideInner}>
                <div className={styles.infoContainer}>
                  <a href={item.link} className={styles.title}>{item.title}</a>
                  <p className={styles.subtitle}>{item.description}</p>
                  <a href={item.link} className={styles.linkButton}>Узнать больше</a>
                </div>
                <div className={styles.videoContainer}>
                  <div
                    className={styles.preview}
                    style={{ backgroundImage: `url("${item.video_preview_url}")`}}
                  >
                    <Button className={styles.playButton}>
                      <Icon type="play"/>
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          )}
        </Slider>
      </div>
    )
  }
}

export default TopCarousel;
