import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import moment from 'moment';
import Slider from 'react-slick';

import Responsive, { tablet, desktop, wide } from 'components/Responsive';
import Icon from 'components/Icon';
import Image from 'components/Image';
import styles from './RecentProjects.module.scss';
import { recentProjects as data } from 'api/mock.js';


class RecentProjects extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
  }

  static defaultProps = {
    className: ''
  }

  componentDidMount() {
    setTimeout(this.recalculate, 500);
    window.addEventListener('resize', this.recalculate);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.recalculate);
  }

  recalculate = () => {
    const slides = Array.from(this.recentProjects.querySelectorAll('.slick-slide'));
    slides.forEach(item => {
      const rect = item.getBoundingClientRect();
      const isSlideVisible = rect.left > 0 && (rect.left + rect.width < this.recentProjects.clientWidth);
      item.classList.toggle(styles.isBlurred, !isSlideVisible);
    })
  }

  setRef = ref => {
    this.recentProjects = ref;
  }

  render() {
    const { className } = this.props;
    const sliderSettings = {
      dots: false,
      arrows: false,
      speed: 300,
      infinite: false,
      autoplay: false,
      slidesToShow: 4,
      slidesToScroll: 4,
      afterChange: this.recalculate,
      responsive: [{
        breakpoint: wide-1,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        }
      }, {
        breakpoint: desktop-1,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          // dots: true,
        }
      }, {
        breakpoint: tablet-1,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      }]
    }

    return (
      <div className={cn(className, styles.recentProjects)} ref={this.setRef}>
        <div className={styles.header}>
          <div className={styles.headerTitle}>Недавно запустились</div>
          <a className={styles.headerLink} href="#recent-projects">
            <Responsive isMobile>
              <span className={styles.headerLinkText}>Еще</span>
            </Responsive>
            <Responsive isTablet isDesktop isWide>
              <span className={styles.headerLinkText}>Новые проекты</span>
              <Icon className={styles.headerLinkIcon} type="arrowRight" />
            </Responsive>
          </a>
        </div>
        <Slider {...sliderSettings}>
          {data.map(item =>
            <div className={styles.slide} key={item.id}>
              <div className={styles.slideInner}>
                <a className={styles.imageLink} href={item.link}>
                  <Image className={styles.image} src={item.image_url} />
                </a>
                <div className={styles.status}>
                  <div
                    className={styles.statusIndicator}
                    style={{ width: `${item.status}%` }}
                  ></div>
                </div>
                <a className={styles.titleLink} href={item.link}>
                  {item.title}
                </a>
                <div className={styles.date}>
                  {moment(item.date).format('DD MMMM')}
                </div>
              </div>
            </div>
          )}
        </Slider>
      </div>
    )
  }
}

export default RecentProjects;
