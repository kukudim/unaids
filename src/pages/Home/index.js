import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import TopCarousel from './TopCarousel';
import RecentProjects from './RecentProjects';
import SubmitProject from './SubmitProject';
import styles from './Home.module.scss';


class Home extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
  }

  static defaultProps = {
    className: ''
  }

  render() {
    const { className } = this.props;
    return (
      <div className={cn(className, styles.page)}>
        <section className={cn(styles.section, styles.sectionTopCarousel)}>
          <div className={styles.sectionInner}>
            <TopCarousel />
          </div>
        </section>
        <section className={cn(styles.section, styles.sectionRecentProjects)}>
          <RecentProjects />
        </section>
        <section className={cn(styles.section, styles.sectionSubmitProject)}>
          <div className={styles.sectionInner}>
            <SubmitProject />
          </div>
        </section>
      </div>
    )
  }
}

export default Home;
