import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import icons from './icons';
import styles from './Icon.module.scss';


class Icon extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
  }

  static defaultProps = {
    className: ''
  }

  render() {
    const { type, className } = this.props;
    return (
      <svg
        className={cn(className, styles.icon)}
        xmlns="http://www.w3.org/2000/svg"
        width={icons[type].width}
        height={icons[type].height}
        viewBox={icons[type].viewBox}
        dangerouslySetInnerHTML={{__html: icons[type].path}}
      >
      </svg>
    )
  }
}

export default Icon;
