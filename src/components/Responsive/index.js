import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';


export const mobile = 320;
export const tablet = 765;
export const desktop = 1020;
export const wide = 1440;

class Responsive extends PureComponent {
  static propTypes = {
    isMobile: PropTypes.bool,
    isTablet: PropTypes.bool,
    isDesktop: PropTypes.bool,
    isWide: PropTypes.bool,
  };

  static defaultProps = {
    isMobile: false,
    isTablet: false,
    isDesktop: false,
    isWide: false,
  }

  render() {
    const { isMobile, isTablet, isDesktop, isWide, children } = this.props;
    let computedQuery = '';
    computedQuery += isMobile ? `(min-width: ${mobile}px) and (max-width: ${tablet-1}px), ` : '';
    computedQuery += isTablet ? `(min-width: ${tablet}px) and (max-width: ${desktop-1}px), ` : '';
    computedQuery += isDesktop ? `(min-width: ${desktop}px) and (max-width: ${wide-1}px), ` : '';
    computedQuery += isWide ? `(min-width: ${wide}px), ` : '';
    computedQuery = computedQuery.slice(0, -2);

    return (
      computedQuery
        ? <MediaQuery query={computedQuery}>
            {children}
          </MediaQuery>
        : <Fragment>
            {children}
          </Fragment>
    )
  }
}

export default Responsive
