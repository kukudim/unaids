import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import styles from './Image.module.scss';


class Image extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    src: PropTypes.string.isRequired,
    alt: PropTypes.string,
    title: PropTypes.string,
  }

  static defaultProps = {
    className: '',
    alt: '',
    title: '',
  }

  render() {
    const { src, alt, title, className } = this.props;
    return (
      <img
        className={cn(className, styles.image)}
        src={src}
        alt={alt}
        title={title}
      />
    )
  }
}

export default Image;
