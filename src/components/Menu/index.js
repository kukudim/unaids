import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import MediaQuery from 'react-responsive';

import { tablet, desktop } from 'components/Responsive';
import Icon from 'components/Icon';
import Button from 'components/Button';
import styles from './Menu.module.scss';

import { headerMenu as data } from '../../api/mock';


const countItemsToShow = 2;

class Menu extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
  }

  static defaultProps = {
    className: '',
  }

  state = {
    isShowMoreMenu: false,
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleBlur);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleBlur);
  }

  setMoreRef = ref => {
    this.moreMenu = ref;
  }

  handleBlur = event => {
    if (this.moreMenu && !this.moreMenu.contains(event.target)) {
      this.setState({ isShowMoreMenu: false });
    }
  }

  toggleMoreMenu = () => {
    this.setState({ isShowMoreMenu: !this.state.isShowMoreMenu })
  }

  handleLinkClick = () => {
    this.setState({ isShowMoreMenu: false })
  }

  renderMoreMenu = (countSkip) => {
    const { isShowMoreMenu } = this.state;

    return (
      <li className={styles.more} ref={this.setMoreRef}>
        <Button className={styles.moreButton} onClick={this.toggleMoreMenu}>
          <Icon type="menuMore"/>
        </Button>
        <ul className={cn(styles.moreList, {[styles.moreListIsOpen]: isShowMoreMenu})}>
          {data.slice(countSkip).map((mItem, mIndex) =>
            <li className={styles.moreListItem} key={mIndex}>
              <a
                className={styles.moreListItemLink}
                href={mItem.link}
                onClick={this.handleLinkClick}
              >
                {mItem.title}
              </a>
            </li>
          )}
        </ul>
      </li>
    )
  }

  render() {
    const { className } = this.props;
    const countAll = data.length;

    return (
      <nav className={cn(className, styles.menu)}>
        <MediaQuery minWidth={tablet} maxWidth={desktop-1}>
          {(isTablet) => {
            const countToShow = isTablet ? countItemsToShow : countAll;
            return (
              <ul className={styles.list}>
                {data.slice(0, countToShow).map((item, index) =>
                  <li className={styles.listItem} key={index}>
                    <a className={styles.listItemLink} href={item.link}>
                      {item.title}
                    </a>
                  </li>
                )}
                {isTablet && countToShow < countAll &&
                  this.renderMoreMenu(countToShow)
                }
              </ul>
            )
          }}
        </MediaQuery>
      </nav>
    )
  }
}

export default Menu;
