import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import Responsive from 'components/Responsive';
import Icon from 'components/Icon';
import Button from 'components/Button';
import Menu from 'components/Menu';
import styles from './Header.module.scss';


class Header extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
  }

  static defaultProps = {
    className: ''
  }

  render() {
    const { className } = this.props;

    return (
      <header className={cn(className, styles.header)}>
        <div className={styles.headerInner}>
          <a href="/" className={styles.logo}>
            <Responsive isMobile>
              <Icon type="logoMobile" />
            </Responsive>
            <Responsive isTablet isDesktop>
              <Icon type="logoTablet" />
            </Responsive>
            <Responsive isWide>
              <Icon type="logo" />
            </Responsive>
          </a>
          <Menu />
          <a href="/en" className={styles.langLink}>EN</a>
          <Button buttonStyle="button" className={styles.logInButton}>
            Войти
          </Button>
          <Button className={styles.mobileMenuButton}>
            <Icon type="menuBurger" />
          </Button>
        </div>
      </header>
    )
  }
}

export default Header;
