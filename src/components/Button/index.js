import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import styles from './Button.module.scss';


class Button extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    classNameInner: PropTypes.string,
    type: PropTypes.string,
    buttonStyle: PropTypes.string,
    appearance: PropTypes.string,
  }

  static defaultProps = {
    className: '',
    classNameInner: '',
    type: 'button',
    buttonStyle: '',
    appearance: ''
  }

  render() {
    const { type, buttonStyle, appearance, className, classNameInner,
      children, onClick } = this.props;
    const computedClassName = cn(className, styles.base, {
      [styles[buttonStyle]]: buttonStyle,
      [styles[appearance]]: appearance,
    });
    return (
      <button
        className={computedClassName}
        type={type}
        onClick={onClick}
      >
        <span className={cn(classNameInner, styles.inner)}>
          {children}
        </span>
      </button>
    )
  }
}

export default Button;
