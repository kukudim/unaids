# Test task built with React ([Live demo](https://unaids-6b8f0.firebaseapp.com/))

## Available Scripts

In the project directory, you can run:

### `npm start`

### `npm test`

### `npm run build`

### `npm run eject`
